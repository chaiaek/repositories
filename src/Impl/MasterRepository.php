<?php


namespace Chaiaek\Repositories\Impl;


use Chaiaek\Repositories\MasterInterface;
use Illuminate\Support\Collection;

class MasterRepository extends BaseRepository implements MasterInterface
{
    public function all() : Collection {
        return $this->model->get();
    }

}
